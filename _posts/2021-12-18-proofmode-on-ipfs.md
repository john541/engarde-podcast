---
layout: post
title: "ProofMode on the InterPlanetary File System"
date: 2021-12-20 10:30:00 -500
file: https://guardianproject.info/podcast-audio/proofmode-on-ipfs.mp3
description: "We are bringing our work on smartphone photos and video as 'Proof' to the decentralized web"
summary: "We are bringing our work on smartphone photos and video as 'Proof' to the decentralized web"
duration: "28:39" 
length: "24090804"
explicit: "no" 
keywords: "ipfs, decentralized web, ipns, filecoin, web3, proofmode"
block: "no" 
voices: "Nathan of Guardian Project"

---

### About this Episode

Nathan talks through his thoughts on new funding and support from the Filecoin Foundation for the Decentralized Web, the upcoming work on ProofMode, journey into the DWeb, how and where to build in the privacy and security needed for activists and journalists, and finishes with a grand unification theory across multiple apps and projects.

### Show Notes and Links

* [FFDW and Guardian Project Team Up to Bring Decentralized Storage to Content Verification and Distribution on Smartphones](https://medium.com/@FFDWeb/ffdw-and-guardian-project-team-up-to-bring-decentralized-storage-to-content-verification-and-f539a4e67ac0)
* [Filecoin Foundation for the Decentralized Web](https://ffdweb.org/)
* [ProofMode.org](https://proofmode.org)
* Nathan is @n8fr8 in many places online, including @n8fr8:matrix.org, and available on email at [nathan@guardianproject.info](mailto:nathan@guardianproject.info) 


