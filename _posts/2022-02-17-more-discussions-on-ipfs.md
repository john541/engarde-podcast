---
layout: post
title: "Fabiola and David discuss IPFS, Filecoin and the Decentralized Web"
date: 2022-02-17 12:30:00 -500
file: https://guardianproject.info/podcast-audio/news-on-the-dweb.mp3
description: "A discussion covering details on decentralized web technologies that we are exploring at Guardian Project"
summary: "A discussion covering details on decentralized web technologies that we are exploring at Guardian Project"
duration: "27:42" 
length: "9969798"
explicit: "no" 
keywords: "ipfs, decentralized web, ipns, filecoin, web3, proofmode"
block: "no" 
voices: "Fabiola and David of Guardian Project"

---

### About this Episode

On this podcast, David Oliver of Guardian Project will talk about the Interplanetary File System (IPFS). David did some investigative work on IPFS for a news dissemination project.

Guardian Project is currently working with Filecoin Foundation for the Decentralized Web to imagine ProofMode and FDroid in a decentralized environment.  

We asked David to give us a high-level overview of IPFS and how it might impact our efforts on Internet freedom and human rights.

[Watch the video version on YouTube](https://www.youtube.com/watch?v=xkF2Pp5TJtg)

### Show Notes and Links

* [F-Droid: Decentralizing Distribution](https://f-droid.org/en/2022/02/05/decentralizing-distribution.html)
* [FFDW and Guardian Project Team Up to Bring Decentralized Storage to Content Verification and Distribution on Smartphones](https://medium.com/@FFDWeb/ffdw-and-guardian-project-team-up-to-bring-decentralized-storage-to-content-verification-and-f539a4e67ac0)
* [Filecoin Foundation for the Decentralized Web](https://ffdweb.org/)
* [IPFS](https://ipfs.io)


