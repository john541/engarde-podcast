---
layout: post
title: "Clean Insights: Arturo Filastò of OONI, on Measuring the Internet Like an Octopus"
date: 2020-05-12 10:30:00 -500
file: https://guardianproject.info/podcast-audio/cise-podcast-2-ooni.mp3
summary: "Nathan and Arturo talk through the promise and peril of uncovering the hidden truths that lurk below!"
description: "Nathan and Arturo talk through the promise and peril of uncovering the hidden truths that lurk below!"
duration: "39:22" 
length: "27,858,405"
explicit: "no" 
keywords: "ooni, network interference, clean insights, privacy preserving measurement, ethical analytics, open-source"
block: "no" 
voices: "Nathan Freitas (n8fr8), Arturo Filastò (OONI.org)"

---

### About this Episode

![ooni header](/podcast/images/posts/ooniheader1.png)
![ooni header](/podcast/images/posts/cise-podcast2-ooni-participants.jpg)

Arturo is a developer at GlobaLeaks and The Tor Project. He studied Mathematics and is currently student of Computer Science at Università di Roma “La Sapienza”. He is a well known security researcher and regularly gives lectures at international conferences. He has trained activists in the use of security and censorship circumvention technologies. He is also the project lead of OONI (Open Observatory of Network Interference), a project aimed at detecting and monitoring internet censorship around the world.

In this discussion, I pose, more or less, the following questions to Arturo Filastò, project lead at the Open Observatory of Network Interference, aka OONI:

* OONI provides benefit but with risk. How do you communicate these to the people doing the measuring or being measured? How does user experience and consent play a role?
* Within an internet freedom and human rights context, are there additional threats and risks to consider? Is measuring itself a crime in some places?
* What do you do with the data that OONI collects? How long do you keep it? Is it public or private? Can it cause harm?
* Who does OONI benefit and how? Is it just an advocacy tool, or does it have technical impact in the design of other services and apps?

All of this was asked in context of our three areas of focus ("Data, Design and Developers!") for the ongoing [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event).

Music courtesy of Archive.org: [Here Comes The Circus](http://archive.org/details/HereComesTheCircus)

### Show Notes and Links

- [Open Observatory of Network Interference (OONI)](https://ooni.org)
- Check-out the Informed Consent "Pop Quiz" --> [Install the OONI Probe Mobile app](https://ooni.org/install/mobile)

![ooni popups for informed consent](/podcast/images/posts/cise-podcast2-ooni-popquiz.jpg)
- Join us in May and June for the [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event)

