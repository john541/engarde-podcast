---
layout: post
title: "Do not turn away in difficult moments"
date: 2020-06-01 5:30:00 -500
file: https://guardianproject.info/podcast-audio/EnGardeNathanonaSundayinMay2020.mp3
summary: "Some personal thoughts on our duty to bear witness, to make positive change, and to use the privilege and power you have to benefit those without"
description: "Some personal thoughts on our duty to bear witness, to make positive change, and to use the privilegee and power you have to benefit those without"
duration: "9:44" 
length: "9448587"
explicit: "no" 
keywords: "human rights, justice, civil rights, racism, police violence, black lives matter"
block: "no" 
voices: "Nathan Freitas (n8fr8)"
---

### About this Episode

Thanks to a random act of COVID-inspired vandalism, I found myself with precious time alone to think about the murder of George Floyd. I hit the record button, mostly to capture the moment, and perhaps to share it with my family and colleagues.

After listening to my 10 minutes of consciousness stream a few times, and finding inspiration from others who are using the platforms they have to speak out, I've decided to share it on our podcast here.  As an organization focused on empowering people to fight for their rights through security and private technology, this message and moment is connected absoutely to the core of what we do, and who we are.  

Thank you for listening, and for any actions taken in your own life, to bear witness, make positive change, and use the privilege and power you have to benefit those without.

![george floyd](https://guardianproject.info/podcast/images/georgefloydrip.jpg)

Photo by [Xena Goldman](https://medium.com/@BarackObama/how-to-make-this-moment-the-turning-point-for-real-change-9fa209806067)




