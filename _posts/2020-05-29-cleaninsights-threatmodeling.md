---
layout: post
title: "Clean Insights: Threat Modeling For Measurement and Analytics"
date: 2020-05-29 5:30:00 -500
file: https://guardianproject.info/podcast-audio/CleanInsights-ThreatModelingForMeasurement.mp3
summary: "Remember, when it comes to implementing measurement and analytics, YOU ARE THE ADVERSARY!"
description: "Remember, when it comes to implementing measurement and analytics, YOU ARE THE ADVERSARY!"
duration: "17:15" 
length: "16551018"
explicit: "no" 
keywords: "clean insights, privacy preserving measurement, ethical analytics, open-source, threat modeling"
block: "no" 
voices: "Nathan Freitas (n8fr8)"
---

### About this Episode

Another talk from the Clean Insights Symposium Extraordinaire.... Remember, when it comes to implementing measurement and analytics, YOU ARE THE ADVERSARY! Nathan (n8fr8) talks through all the ways you can fail your users if you instrument their activity too much, or don't take care of the resulting data.

[Download and view the PDF presentation](https://cleaninsights.org/assets/docs/CleanInsights-ThreatModelingForMeasurementandAnalytics.pdf) that accompanies this podcast talk.

You can watch a video of this talk with slides on [YouTube](https://www.youtube.com/watch?v=nt6ac3WoogA&feature=youtu.be) or the [Internet Archive](https://archive.org/details/clean-insights-threat-modeling-for-measurement)

This podcast talk is part of the [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event)

[![image of talk](https://cleaninsights.org/assets/images/talk-threatmodeling.jpg)](https://www.youtube.com/watch?v=nt6ac3WoogA&feature=youtu.be)



