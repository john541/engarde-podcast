---
layout: post
title: "Queer DWeebs Squad Pod ⭐"
date: 2022-11-01 12:30:00 -500
file: https://guardianproject.info/podcast-audio/QueerDWeebsSquadPod-October2022.mp3
description: "One group's perspective on the pros and cons of inclusivity at DWeb Camp 2022"
summary: "One group's perspective on the pros and cons of inclusivity at DWeb Camp 2022"
duration: "1:08:06" 
length: "61018107"
explicit: "no" 
keywords: "dwebcamp, queer, inclusivity"
block: "no" 
voices: "Mai, Christine, Morgan, Shafali, Jack, Gabe, Dmitri"

---

### About this Episode

The following podcast is one group’s perspective on the pros and cons of inclusivity at DWeb Camp 2022.

DWeb Camp is a 5 day retreat for hackers, builders, and dreamers to gather in nature and tackle the real world problems facing the web and to co-create the decentralized technologies of the future

It was described by one of the founders as the Chaos Communication Congress meets Burning Man. The idea is to come together collectively to exchange ideas and make connections.

You do NOT have to be a tech person to go to DWeb camp, just someone who cares.

### Show Notes and Links

* [DWebCamp](https://dwebcamp.org/)

Participants
- Mai: mai ishikawa sutton @maira (twitter) [maisutton.net](https://maisutton.net)
- Christine
- Morgan
- Shafali
- Gabe
- Dmitri Zagidulin: @codenamedmitri (twitter) / [https://chaos.social/web/@dmitri - Fediverse](https://chaos.social/web/@dmitri)
- Jack Fox Keen: @QueenOfJackFox (twitter) / [https://www.linkedin.com/in/jack-fox-keen/](https://www.linkedin.com/in/jack-fox-keen/)

Music: Wonder Happens by Podington Bear is licensed under a Attribution-NonCommercial 3.0 International License.
[freemusicarchive.org](https://freemusicarchive.org/music/Podington_Bear/Meet_Podington_Bear_Box_Set_Disc_1/Wonder_Happens/)
