---
layout: post
title: "Onions on Apples: Mike Tigas on bringing Tor to iOS"
date: 2020-06-25 18:30:00 -400
file: https://guardianproject.info/podcast-audio/engarde_podcast_onionbrowser_june2020.mp3
summary: "Bringing Tor Browser functionality to iPhones isn't an easy job. That has never stopped Mike Tigas, lead developer of Onion Browser, from trying."
description: "Bringing Tor Browser functionality to iPhones isn't an easy job. That has never stopped Mike Tigas, lead developer of Onion Browser, from trying."
duration: "20:27" 
length: "24049991"
explicit: "no" 
keywords: "tor, proxy, circumvention, iOS, apple, iPhone"
block: "no" 
voices: "Fabiola Maurice (GP), Mike Tigas (GP)"
---

### About this Episode

Fabiola of the Guardian Project, talks with Mike Tigas, original and lead developer of Onion Browser for iOS. Onion Browser is the closest you can get to having Tor Browser on an iPhone or iPad device. The most recent improvements in the 2.x versions of the app have been built in collaboration with Guardian Project and Okthanks. Listen to Fabiola and Mike talk about the origins, challenges, updates and roadmap ahead for the work with Tor on iOS, or as we call it "Onions on Apples".

### Show Notes and Links

- [mike.tig.as homepage](https://mike.tig.as/)
- [Onion Browser homepage](https://onionbrowser.com)
- [Onion Browser v2.6 Tutorial](https://guardianproject.info/2020/06/02/onion-browser-release-2.6-tutorial/)
- [Onion Browser video tutorials on YouTUbe](https://www.youtube.com/watch?v=qooaCB_fXSo&list=PL4-CVUWabKWeHeBpadBLjzlWVa7binXpj)
- [Onion Browser Open-Source Project on Github](https://github.com/OnionBrowser/OnionBrowser)


