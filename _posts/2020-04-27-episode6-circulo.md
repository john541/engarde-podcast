---
layout: post
title: "Círculo: Stay Safe Together"
date: 2020-04-27 10:30:00 -500
file: https://guardianproject.info/podcast-audio/engard-circulo-april2020-english.mp3
summary: "In this episode, Fabby, our Community Manager, is joined by Vladimir Cortez and Martha Tudon from the Article 19 Office in Mexico to talk about Circulo"
description: "In this episode, Fabby, our Community Manager, is joined by Vladimir Cortez and Martha Tudon from the Article 19 Office in Mexico to talk about Circulo"
duration: "15:36" 
length: "1735145"
explicit: "no" 
keywords: "physical safety, jouranlists, mexico, circulo, panic button"
block: "no" 
voices: "Fabiola Maurice, Vladimir Cortez, Martha Tudon"
---

### About this Episode

In this episode, Fabby, our Community Manager, is joined by Vladimir Cortez and Martha Tudon from the Article 19 Office in Mexico to talk about Circulo, a collaborative effort between Article 19, OkThanks and Guardian Project to develop a digital tool for female journalists in Mexico that could help them create strong networks of support,  strengthen their safety protocols and meet all their needs for transparency,  privacy, anonymity, and reliability. It was a two-year-long process working hand to hand with our end-users through chat groups and also several in-person workshops to make sure everything built in the app was something they needed and could trust. Listen to the audio for the whole story, and learn more about the project, the challenges we faced, and what it is to come.

Watch the full conversation on the [Guardian Project YouTube Channel](https://youtu.be/NN1lu6P1fE0)

![fabby](https://guardianproject.info/podcast-audio/engarde-circulo.jpg)


### Show Notes and Links

- [Article 19](https://article19.org)
- [Circulo Website](https://encirculo.org/index.en.html)
- [Circulo video tutorial - English](https://www.youtube.com/watch?v=zxOxkud3nVw&feature=emb_title)
- [Circulo Android Open-Source Project](https://gitlab.com/circuloapp/circulo-android)


